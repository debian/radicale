radicale (2.1.11-4) unstable; urgency=medium

  * Radicale 2.x has several incompatible changes from Radicale 1.x.

    Most importantly, data stored with Radicale 1.x is unreadable,
    and need to be exported using Radicale 1.1.6 before switching to
    Radicale 2.x.
    A suitable debian package can be temporarily installed from
    <https://snapshot.debian.org/package/radicale/1.1.6-4/>,
    which tries backup to </var/backups/radicale-*/radicale-data.tar.gz>
    for manual restore to </var/lib/radicale/collections/collection-root>.
    More information at <https://radicale.org/1to2/>.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 07 Feb 2019 02:28:00 +0100

radicale (2.1.10-1) experimental; urgency=medium

  * radicale can no longer be enabled in </etc/default/radicale>.
    Enable it using the command 'update-rc.d radicale enable'.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 23 Sep 2018 14:48:46 +0200

radicale (1.1.1-1) unstable; urgency=medium

  * Use the first matching section for rights

    Now, the first section matching the path and current user in your
    custom rights file is used.  In the previous versions, the most
    permissive rights of all the matching sections were applied.  This
    new behaviour gives a simple way to make specific rules at the top
    of the file independent from the generic ones.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 12 Jan 2016 09:17:35 +0530

radicale (0.7-1) unstable; urgency=low

    This package version includes two important changes:


    Changed config variable for calendar folder
    ===========================================

    Radicale now supports different storage backends as plugins. This led to a
    variable name change in the configuration file, namely "folder" changed to
    "filesystem_folder". You need to adjust your config file accordingly.


    System daemon
    =============

    Starting with this version, the radicale Debian package defaults to
    running the server in system daemon mode using a supplied initscript.
    During package installation, a system user "radicale" was created and
    creation of relevant system directories was attempted. The default
    directories are:

    /var/lib/radicale - for calendars and contacts,
    /var/log/radicale - for log files,
    /var/run/radicale - for run files, e.g., the pid file.

    The supplied config files /etc/radicale/config and /etc/radicale/logging
    are now pointing to those directories by default.

    You can continue running radicale as an ordinary user if you have done so
    previously, in which case you only need to adjust the config file to point
    to the old location of your calendar files (see variable renaming above).
    However, if you wish to migrate to the new locations in order to run
    radicale as a system daemon, you can use the following procedure:


    1.) Stop the server if it is running.


    2.) Determine the current location of calendar files and log file 
    from, e.g., /etc/radicale/config and /etc/radicale/logging or your custom
    command line for launching radicale. In the following we assume that:

    * ~/.config/radicale/calendars is the old location of your calendars, and

    * /var/log/radicale is the old log file.

    (Both were the defaults in previous versions of this Debian package.)


    3.) The following steps require root privileges. Move the calendar data
    to its new location:

        # mv /home/<youruser>/.config/radicale/calendars /var/lib/radicale/collections

    (Since radicale can now handle contact data as well, the directory is
    called "connections" from now on.) The collections need to be owned by the
    radicale user and group:

        # chown -R radicale:radicale /var/lib/radicale/collections/

    You may want to remove the old and now empty directory
    /home/<youruser>/.config/radicale/ .


    4.) For migrating the log file, we again need root privileges. First,
    rename the old log file:

        # mv /var/log/radicale /var/log/radicale.log

    Second, create the new log directory:

        # mkdir /var/log/radicale

    Third, move the log file into the log directory:

        # mv /var/log/radicale.log /var/log/radicale/

    Again, the files need to be owned by the radicale user and group:

        # chown -R radicale:radicale /var/log/radicale/

    Note that if you have logrotate installed, it will now be used to handle
    the log files of radicale in that directory.


    5.) Check whether /etc/radicale/config and /etc/radicale/logging suit your
    new site configuration including locations.


    6.) Activate the daemon in /etc/default/radicale. You can also supply
    additional startup options there. Now, start the daemon using:

        # /etc/init.d/radicale start


 -- Martin Stigge <martin@stigge.org>  Sun, 08 Apr 2012 17:11:24 +0200
